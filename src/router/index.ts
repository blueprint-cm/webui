import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";

import Users from "../views/Users.vue";
import Devices from "../views/Devices.vue";

import NewUser from "../views/users/NewUser.vue";
import SearchUser from "../views/users/SearchUser.vue";
import NewUsrGroup from "../views/users/NewUsrGroup.vue";
import SearchUsrGroup from "../views/users/SearchUsrGroup.vue";

import NewDevice from "../views/devices/NewDevice.vue";
import SearchDevice from "../views/devices/SearchDevice.vue";
import NewDevGroup from "../views/devices/NewDevGroup.vue";
import SearchDevGroup from "../views/devices/SearchDevGroup.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/users",
        name: "Users",
        component: Users,
        children: [
            { path: "/users/newuser", component: NewUser },
            { path: "/users/searchuser", component: SearchUser },
            { path: "/users/newgroup", component: NewUsrGroup },
            { path: "/users/searchgroup", component: SearchUsrGroup}
        ],
    },
    {
        path: "/devices",
        name: "Devices",
        component: Devices,
        children: [
            { path: "/devices/newdevice", component: NewDevice },
            { path: "/devices/searchdevice", component: SearchDevice },
            { path: "/devices/newgroup", component: NewDevGroup },
            { path: "/devices/searchgroup", component: SearchDevGroup}
        ],
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;
